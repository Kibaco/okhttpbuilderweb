package com.asiainfo.http;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * 搭建web提交表单的服务
 */
@MultipartConfig
@WebServlet(name = "UploadServlet", urlPatterns = "/UploadServlet")
public class UploadServlet extends HttpServlet {

    public UploadServlet() {
        super();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        doGet(request, response);

        String name = request.getParameter("name");
        Part part = request.getPart("filename");
        PrintWriter out = response.getWriter();
        out.println("name: " + name + "\n");
        out.println("ContentType: " + part.getContentType() + "\n");
        out.println("Size: " + part.getSize() + "\n");
        out.println("/Users/MicroKibaco/girl_server.jpg");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        response.getWriter().append("Served at: ").append(request.getContextPath() + "\n");


    }
}
