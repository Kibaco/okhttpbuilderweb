package com.asiainfo.http;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 搭建web提交表单的服务
 */
@WebServlet(name = "HelloServlet" ,urlPatterns = "/HelloServlet")
public class HelloServlet extends HttpServlet {

    public HelloServlet() {
        super();
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        doGet(request, response);
        String username = request.getParameter("username");
        String userage = request.getParameter("userage");
        response.getWriter().
                append("username=").
                append(username + "\n").
                append("userage=").
                append(userage);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.getWriter().append("Served at: ").append(request.getContextPath() + "\n");


    }
}
